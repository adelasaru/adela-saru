const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const cors = require('cors')


const sequelize = new Sequelize('books_authors', 'root', '', {
  dialect: 'mysql',
  define: {
    timestamps: false
  }
})


const Author = sequelize.define('author', {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [2, 20]
    }
  },
  genre: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [2, 60]
    }
  }
}, {
  underscored: true
})
const Book = sequelize.define('book', {
  title: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [2, 30]
    }
  },

  description: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [10, 100]
    }
  }

})



Author.hasMany(Book)

const app = express()
app.use(bodyParser.json())
app.use(express.static('../frontend/build'))
app.use(cors())

app.get('/create', (req, res, next) => {
  sequelize.sync({ force: true })
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

app.get('/authors', (req, res, next) => {
  Author.findAll()
    .then((authors) => res.status(200).json(authors))
    .catch((error) => next(error))
})

app.post('/authors', (req, res, next) => {
  Author.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

app.get('/authors/:id', (req, res, next) => {
  Author.findById(req.params.id, { include: [Book] })
    .then((author) => {
      if (author) {
        res.status(200).json(author)
      }
      else {
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))
})

app.put('/authors/:id', (req, res, next) => {
  Author.findById(req.params.id)
    .then((author) => {
      if (author) {
        return author.update(req.body, { fields: ['name', 'genre'] })
      }
      else {
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

app.delete('/authors/:id', (req, res, next) => {
  Author.findById(req.params.id)
    .then((author) => {
      if (author) {
        return author.destroy()
      }
      else {
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

app.get('/authors/:aid/books', (req, res, next) => {
  Author.findById(req.params.aid)
    .then((author) => {
      if (author) {
        return author.getBooks()
      }
      else {
        res.status(404).send('not found')
      }
    })
    .then((books) => {
      if (!res.headersSent) {
        res.status(200).json(books)
      }
    })
    .catch((err) => next(err))
})

app.post('/authors/:aid/books', (req, res, next) => {
  Author.findById(req.params.aid)
    .then((author) => {
      if (author) {
        let book = req.body
        book.author_id = author.id
        return Book.create(book)
      }
      else {
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('created')
      }
    })
    .catch((err) => next(err))
})

app.get('/authors/:aid/books/:bid', (req, res, next) => {
  Book.findById(req.params.mid)
    .then((book) => {
      if (book) {
        res.status(200).json(book)
      }
      else {
        res.status(404).send('not found')
      }
    })
    .catch((err) => next(err))
})

app.put('/authors/:aid/books/:bid', (req, res, next) => {
  Book.findById(req.params.id)
    .then((book) => {
      if (book) {
        return book.update(req.body, { fields: ['title', 'decription'] })
      }
      else {
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('modified')
      }
    })
    .catch((err) => next(err))
})

app.delete('/authors/:aid/books/:bid', (req, res, next) => {
  Book.findById(req.params.id)
    .then((book) => {
      if (book) {
        return book.destroy()
      }
      else {
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('removed')
      }
    })
    .catch((err) => next(err))
})

app.use((err, req, res, next) => {
  console.warn(err)
  res.status(500).send('some error...')
})

app.listen(8081)
