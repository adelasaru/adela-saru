**Reading lists integrated with Goodreads**

The application might be associated with a blog because I will make multiple lists of books in order to share with other people my preferences.
On my webpage I will present the following features:
- **Favorite books** A list (from Goodreads) of all my favourite books.
- **Preferred authors** Also a list with authors.
- **Recommendations & personal reviews** 
**Important** 
Favorite books & Preferred authors will be only lists, but when I click on them I can see official reviews and ratings from Goodreads.
Also for every book in the list I can share my personal opinions and why I recommend a certain book or authors.
And besides the ratings from Goodreads I can also rate my books.
In addition, the frontend part's purpose is to make the page attractive. The reading lists will be displayed in a way that the user will be able
to choose very quick the information he/she needs.


