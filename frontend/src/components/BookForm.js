import React, { Component } from 'react'
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';

class BookForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            bookTitle: '',
            bookDescription: ''
        }
        this.handleChange = (event) => {
            this.setState({
                [event.target.name]: event.target.value
            })
        }
    }
    render() {
        return (
            <Form>
        <Input type="text" placeholder="Title" name="bookTitle"  onChange={this.handleChange}/>
        <Input type="text" placeholder="Description" name="bookDescription"  onChange={this.handleChange} />
        <Button type="button" value="add" variant="raised" onClick = {
                () => this.props.onAdd({ title: this.state.bookTitle, description: this.state.bookDescription }) }>Submit</Button>
      </Form>
        )
    }
}

export default BookForm
