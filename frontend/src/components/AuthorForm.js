import React, { Component } from 'react'
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';


class AuthorForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            authorName: '',
            authorGenre: ''
        }
        this.handleChange = (event) => {
            this.setState({
                [event.target.name]: event.target.value
            })
        }
    }

    render() {
        return (
            <Form>
        <Input type="text" placeholder="Name" name="authorName"  onChange={this.handleChange}/>
        <Input type="text" placeholder="Genre" name="authorGenre"  onChange={this.handleChange} />
        <Button type="button" value="add" variant="raised" onClick = {
                () => this.props.handleAdd({ name: this.state.authorName, genre: this.state.authorGenre }) }>Submit</Button>
      </Form>
        );
    }
}



export default AuthorForm
