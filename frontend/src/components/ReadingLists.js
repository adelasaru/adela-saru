import React, { Component } from 'react'
import AuthorStore from '../stores/AuthorStore'
import { EventEmitter } from 'fbemitter'
import Author from './Author'
import AuthorForm from './AuthorForm'
import BookList from './BookList'
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';

let ee = new EventEmitter()
let store = new AuthorStore(ee)

function addAuthor(author) {
    store.addOne(author)
}

function deleteAuthor(id) {
    store.deleteOne(id)
}

function saveAuthor(id, author) {
    store.saveOne(id, author)
}

class ReadingLists extends Component {
    constructor(props) {
        super(props)
        this.state = {
            authors: [],
            detailsFor: -1,
            selected: null
        }

        this.cancelSelection = () => {
            this.setState({
                detailsFor: -1
            })
        }
        this.selectAuthor = (id) => {
            store.getOne(id)
            ee.addListener('SINGLE_AUTHOR_LOAD', () => {
                this.setState({
                    detailsFor: store.selected.id,
                    selected: store.selected
                })
            })
        }
    }
    componentDidMount() {
        store.getAll()
        ee.addListener('AUTHOR_LOAD', () => {
            this.setState({
                authors: store.content
            })
        })
    }
    render() {
        if (this.state.detailsFor === -1) {
            return (<div>
        {
          this.state.authors.map((e) => <Author author={e} onDelete={deleteAuthor} key={e.id} onSave={saveAuthor} onSelect={this.selectAuthor} />)
        }
        <AuthorForm handleAdd={addAuthor}/>
      </div>)
        }
        else {
            return (
                <div>
          <BookList author={this.state.selected} />
          <Button type="button" variant="raised" value="back" onClick={() => this.cancelSelection()}>Back</Button>
        </div>
            )
        }
    }
}

export default ReadingLists
