import React, { Component } from 'react';
//import logo from './logo.svg';
import '../style/App.css';
import ReadingLists from './ReadingLists'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      forChild: 'some content'
    }
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title"></h1>
      </header>
         <h1>Reading Lists</h1>
        
        <ReadingLists />
        
      </div>
    );
  }
}

export default App;
