import React, { Component } from 'react'

import Button from 'muicss/lib/react/button';
import Input from 'muicss/lib/react/input';

class Author extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isEditing: false,
            author: this.props.author,
            authorName: this.props.author.name,
            authorGenre: this.props.author.genre
        }
        this.handleChange = (event) => {
            this.setState({
                [event.target.name]: event.target.value
            })
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({

            author: nextProps,
            authorName: this.props.author.name,
            authorEmail: this.props.author.email,
            isEditing: false
        })
    }

    render() {
        if (this.state.isEditing) {
            return (<div> 
        <Input type="text" placeholder="Name" type="text" name="authorName" value={this.state.authorName} onChange={this.handleChange}/> 
    
        <Input type="text" placeholder="Genre" name="authorGenre" value={this.state.authorGenre} onChange={this.handleChange}/>
        
        <Button type="button" variant= "raised" value="save" onClick={() => this.props.onSave(this.props.author.id, {name : this.state.authorName, genre : this.state.authorGenre})}>Save</Button>
         <Button type="button" variant= "raised" value="cancel" onClick={() => this.setState({isEditing : false})}>Cancel</Button>
      </div>)
        }
        else {
            return (<div>
        {this.state.authorName} writes {this.state.authorGenre} .
         <Button type="button" variant= "raised" value="delete" onClick={() => this.props.onDelete(this.props.author.id)}>Delete</Button>
         <Button type="button" variant= "raised" value="edit" onClick={() => this.setState({isEditing : true})}>Edit</Button>
         <Button type="button" variant= "raised" value="details" onClick={() => this.props.onSelect(this.props.author.id)}>Details</Button>
      </div>)
        }
    }
}

export default Author
