import React, { Component } from 'react'

class Book extends Component {
  render() {
    return (
      <div>
        <h4>{this.props.book.title}</h4>
        {this.props.book.description}
      </div>
    )
  }
}

export default Book
