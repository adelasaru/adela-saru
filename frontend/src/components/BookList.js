import React, { Component } from 'react'
import Book from './Book'
import BookForm from './BookForm'
import BookStore from '../stores/BookStore'
import { EventEmitter } from 'fbemitter'

const ee = new EventEmitter()
const store = new BookStore(ee)

class BookList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            books: []
        }
        this.addBook = (book) => {
            store.addOne(this.props.author.id, book)
        }
        this.deleteBook = (book) => {
            store.deleteOne(this.props.author.id, this.props.book.id)
        }
        this.saveBook = (book) => {
            store.saveOne(this.props.author.id, this.props.book.id, book)
        }

    }
    componentDidMount() {
        store.getAll(this.props.author.id)
        ee.addListener('BOOK_LOAD', () => {
            this.setState({
                books: store.content
            })
        })
    }
    render() {
        return (
            <div>
        Author {this.props.author.name} writes {this.props.author.genre}
        <h2>Books:</h2>
        {
          this.state.books.map((b) => <Book book={b} onDelete={this.deleteBook} key={b.id} onSave={this.saveBook} />)
        }
        <h2>Add a book</h2>
        <BookForm onAdd={this.addBook}/>
      </div>
        )
    }
}

export default BookList
