import axios from 'axios'
const SERVER = 'https://adelasaru-adelasaru.c9users.io'

class BookStore {
    constructor(ee) {
        this.ee = ee
        this.content = []
    }
    getAll(authorId) {
        axios(SERVER + '/authors/' + authorId + '/books')
            .then((response) => {
                this.content = response.data
                this.ee.emit('BOOK_LOAD')
            })
            .catch((error) => console.warn(error))
    }
    addOne(authorId, book) {
        axios.post(SERVER + '/authors/' + authorId + '/books', book)
            .then(() => this.getAll(authorId))
            .catch((error) => console.warn(error))
    }
    deleteOne(authorId, bookId) {
        axios.delete(SERVER + '/authors/' + authorId + '/books/' + bookId)
            .then(() => this.getAll(authorId))
            .catch((error) => console.warn(error))
    }
    saveOne(authorId, bookId, book) {
        axios.put(SERVER + '/authors/' + authorId + '/books/' + bookId, book)
            .then(() => this.getAll(authorId))
            .catch((error) => console.warn(error))
    }
}

export default BookStore
